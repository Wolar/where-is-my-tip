//
//  SlidersUtils.swift
//  WhereIsMyTip
//
//  Created by Martin Novák on 17.06.14.
//  Copyright (c) 2014 Martin Novak. All rights reserved.
//

import UIKit

class SlidersUtils: NSObject {
   
    class func pointPairToBearingDegrees(startingPoint : CGPoint,  endingPoint: CGPoint) -> CGFloat {
        var originPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y); // get origin point to origin by subtracting end from start

#if arch(arm) || arch(i386)
        let bearingRadians = atan2f(originPoint.y, originPoint.x); // get bearing in #endifradians
#endif
        
#if arch(arm64) || arch(x86_64)
        let bearingRadians = atan2(originPoint.y, originPoint.x); // get bearing in #endifradians
#endif
        
        var bearingDegrees = bearingRadians * CGFloat(180.0 / M_PI); // convert to degrees
        bearingDegrees = (bearingDegrees > 0.0 ? bearingDegrees : (360.0 + bearingDegrees)); // correct discontinuity
        return bearingDegrees;
    }
    
}
