//
//  CircleSlider.swift
//  WhereIsMyTip
//
//  Created by Martin Novák on 16.06.14.
//  Copyright (c) 2014 Martin Novak. All rights reserved.
//

import QuartzCore
import UIKit

@IBDesignable
class CircleSlider: UIControl {
    
//MARK: properties
    
    //priavte
    var centerLabel : UILabel?;
    var shapeLayer : CAShapeLayer?;
    var backgroundLayer : CAShapeLayer?;
    
    @IBInspectable var minValue : Double  = 0 {
        didSet {
            self.redrawSlider();
        }
    }
    
    @IBInspectable var maxValue : Double = 100 {
        didSet {
            self.redrawSlider();
        }
    }
    
    @IBInspectable var value : Double  = 50 {
        didSet {
            self.redrawSlider();
        }
    }
    
    @IBInspectable var decimalPlaces : Int = 0 {
        didSet {
            if decimalPlaces < 0 {
                self.decimalPlaces = 0;
            }
            self.redrawSlider();
        }
    }
    
    @IBInspectable var lineColor : UIColor  = UIColor.blueColor() {
        didSet {
            if shapeLayer {
                shapeLayer!.strokeColor = lineColor.CGColor;
            }
            self.redrawSlider();
        }
    }
    
    @IBInspectable var backgroundLineColor : UIColor  = UIColor.grayColor() {
        didSet {
            if backgroundLayer {
                backgroundLayer!.strokeColor = backgroundLineColor.CGColor;
            }
            self.redrawSlider();
        }
    }
    
    @IBInspectable var labelSuffix : String = "" {
        didSet {
            self.redrawSlider();
        }
    }
    
    @IBInspectable var lineWidth : CGFloat  = 10 {
        didSet {
            shapeLayer?.removeFromSuperlayer()
            shapeLayer = nil;
            self.redrawSlider();
        }
    }
    
    //Degrees
    @IBInspectable var lineStart : CGFloat  = 0 {
        didSet {
            shapeLayer?.removeFromSuperlayer()
            shapeLayer = nil;
            self.redrawSlider();
        }
    }
    
    //Degrees
    @IBInspectable var lineLengh : CGFloat  = 360 {
        didSet {
            shapeLayer?.removeFromSuperlayer()
            shapeLayer = nil;
            self.redrawSlider();
        }
    }
    
    @IBInspectable var clockwise : Bool  = true {
        didSet {
            shapeLayer?.removeFromSuperlayer()
            shapeLayer = nil;
            self.redrawSlider();
        }
    }
    
//MARK: rendering
    
    override func layoutSubviews()  {
        self.redrawSlider();
    }
    
    //this function is used to redraw slider
    func redrawSlider() {
        //adds center label
        
        if !centerLabel {
            centerLabel = UILabel(frame:self.bounds);
            centerLabel!.textAlignment = NSTextAlignment.Center;
            self.addSubview(centerLabel);
        }
        //set centers label value
        
        
        if self.labelSuffix.utf16count > 0 {
            centerLabel!.text = String(format: "%.*f", self.decimalPlaces , value)  + " " + self.labelSuffix;
        } else {
            centerLabel!.text = String(format: "%.*f", self.decimalPlaces , value);
        }
        
        /*Image angle crop*/
        if !shapeLayer {
            var shapeLayerLocal = CAShapeLayer();
            let innerRect = CGRectInset(bounds, lineWidth/2.0, lineWidth/2.0);
            var innerPath : UIBezierPath?;
            if clockwise {
                innerPath = UIBezierPath(arcCenter:CGPoint(x: frame.height/2,y: frame.width/2), radius:innerRect.height/2, startAngle: CGFloat(M_PI)*lineStart/180, endAngle: CGFloat(M_PI)*lineLengh/180+CGFloat(M_PI)*lineStart/180, clockwise: clockwise);
            } else {
                innerPath = UIBezierPath(arcCenter:CGPoint(x: frame.height/2,y: frame.width/2), radius:innerRect.height/2, startAngle:CGFloat(M_PI)*lineLengh/180+CGFloat(M_PI)*lineStart/180, endAngle:CGFloat(M_PI)*lineStart/180, clockwise: clockwise);
            }
            
            shapeLayerLocal.path = innerPath!.CGPath;
            shapeLayerLocal.fillColor = nil;
            shapeLayerLocal.lineWidth = lineWidth;
            shapeLayerLocal.strokeColor = lineColor.CGColor;
            shapeLayerLocal.anchorPoint = CGPointMake(0.5, 0.5);
            layer.addSublayer(shapeLayerLocal);
            shapeLayer = shapeLayerLocal;
            
            if backgroundLayer {
                    backgroundLayer!.removeFromSuperlayer();
                    backgroundLayer = nil;
            }
            
            if !backgroundLayer {
                var shapeLayerLocal = CAShapeLayer();
                let innerRect = CGRectInset(bounds, lineWidth/2.0, lineWidth/2.0);
                var innerPath : UIBezierPath?;
                if clockwise {
                    innerPath = UIBezierPath(arcCenter:CGPoint(x: frame.height/2,y: frame.width/2), radius:innerRect.height/2, startAngle: CGFloat(M_PI)*lineStart/180, endAngle: CGFloat(M_PI)*lineLengh/180+CGFloat(M_PI)*lineStart/180, clockwise: clockwise);
                } else {
                    innerPath = UIBezierPath(arcCenter:CGPoint(x: frame.height/2,y: frame.width/2), radius:innerRect.height/2, startAngle:CGFloat(M_PI)*lineLengh/180+CGFloat(M_PI)*lineStart/180, endAngle:CGFloat(M_PI)*lineStart/180, clockwise: clockwise);
                }
                
                shapeLayerLocal.path = innerPath!.CGPath;
                shapeLayerLocal.fillColor = nil;
                shapeLayerLocal.lineWidth = lineWidth-1;
                shapeLayerLocal.strokeColor = backgroundLineColor.CGColor;
                shapeLayerLocal.anchorPoint = CGPointMake(0.5, 0.5);
                layer.insertSublayer(shapeLayerLocal, below: shapeLayer);
                backgroundLayer = shapeLayerLocal;
            }
        }
        if shapeLayer {
            shapeLayer!.strokeEnd = CGFloat(value/maxValue);
        }
    }
    
//MARK: touches
    
    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!) {
        handleTouch(touches);
    }
    
    override func touchesMoved(touches: NSSet!, withEvent event: UIEvent!)  {
        handleTouch(touches);
    }
    
    func handleTouch(touches: NSSet!) {
        let touch : AnyObject! = touches.anyObject();
        let touchLocation = touch.locationInView(self);
        
        let center = CGPointMake(self.frame.size.width/2,self.frame.size.height/2);
        let degrees = SlidersUtils.pointPairToBearingDegrees(center, endingPoint: touchLocation);
        
        let realDegree = (degrees - lineStart + 360)%360;
        
        //TODO: implement clockwise off
        if realDegree/lineLengh <= 1 {
            self.sendActionsForControlEvents(UIControlEvents.ValueChanged);
            value = maxValue*Double(realDegree/lineLengh);
        }
    }
    
    

}
